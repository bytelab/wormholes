Wormholes
=========

Fancy teleportation system to replace generic portals!  

Build Status:  
[![Build Status](https://snapci.com/ByteLab/Wormholes/branch/master/build_image)](https://snapci.com/ByteLab/Wormholes/branch/master)
